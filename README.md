# CI/CD vuln scanning with Cargo Audit
In an attempt to get CI/CD working on GitLab with `cargo-aduit`, we explore the depths of DevOps and the usefulness of dependency scanning. 

## Purpose
Wanted to try out the Pipeline feature of the GitLab. Mostly just used jobs and discovered that they fail automatically if the `cargo audit` tool reports an error. This is likely because it supports Jenkins, where this software is used in an almost identical manner. The file of interest in all of this is not the exploitable code, or even the adivsory database. What needed to be understood is lining up the pipeline itself. This is done with a `.gitlab-ci.yml` file which can be further researched [here](https://docs.gitlab.com/ee/ci/yaml/README.html). Something to note is that this is pretty much using a separate dependency scanner (`cargo audit`) rather than the built in tools that GitLab [provides[]


## About the Exploit
The vulnerable version of [tar](https://github.com/alexcrichton/tar-rs/releases?after=0.4.17) implemented in Rust allows for arbritrary file writes by writing through hard links the tar file. This vulnerable version has been included in this repo and there is also the correct line to use it in the `Cargo.toml` file.

Here's some stuff from the advisory DB:
```
[advisory]
id = "RUSTSEC-2018-0002"
package = "tar"
unaffected_versions = []
patched_versions = [">= 0.4.16"]
keywords = ["file-overwrite"]
url = "https://github.com/alexcrichton/tar-rs/pull/156"
title = "Links in archives can overwrite any existing file"
date = "2018-06-29"
description = """
When unpacking a tarball with the `unpack_in`-family of functions it's intended
that only files within the specified directory are able to be written. Tarballs
with hard links or symlinks, however, can be used to overwrite any file on the
filesystem.
Tarballs can contain multiple entries for the same file. A tarball which first
contains an entry for a hard link or symlink pointing to any file on the
filesystem will have the link created, and then afterwards if the same file is
listed in the tarball the hard link will be rewritten and any file can be
rewritten on the filesystem.
This has been fixed in https://github.com/alexcrichton/tar-rs/pull/156 and is
published as `tar` 0.4.16. Thanks to Max Justicz for discovering this and
emailing about the issue!
"""
```

Here's the repo [comments](https://github.com/alexcrichton/tar-rs/commit/d3d14ad5c32922444f857382976c22f056e3cef4#diff-6ddd650ce033937f353ec15415477cc2)
```
// Remove an existing file, if any, to avoid writing through
// symlinks/hardlinks to weird locations. The tar archive says this is a
// regular file, so let's make it a regular file.
```

Here's the GitHub issue where this was [resolved](https://github.com/alexcrichton/tar-rs/pull/156)
```
This crate tries to provide the guarantee that unpack_in won't actually overwrite any files outside of the provided directory. Unfortunately though there's a hole in this logic where a hard link can be used to overwrite an arbitrary file on the filesystem.

This commit fixes this issue by using the preexisting logic for extracting paths outside the destination (including symlinks) to validate the link destination of a hard link.
```

## TODO
 * Could try and leverage `cache` to make the pipeline job much faster
 * Actually try the exploit, which is confusing because the descriptions don't really specify how it works. Then again, this is probably just me not understanding hard links.


## What got me here
This [list](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html) which led me to [poetry](https://python-poetry.org/docs/) although this has largely been [completed](https://gitlab.com/gitlab-org/gitlab/issues/7006) by this [contributor](https://gitlab.com/janw). After determining that Python would be a no go, I set my sights back on the language that seems ripe for innovation and open source contribution. Rust's dependency scanner `cargo-audit` seems fairly thorough and I imagine it would be the defacto dependency scanner used by the community. However, I'm trying to stay relevant to GitLab, which bought out [Gemnasium](https://about.gitlab.com/press/releases/2018-01-30-gemnasium-acquisition.html) to bump up their [security profile](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium). Not entirely sure if they'll ever support [Rust](https://gitlab.com/gitlab-org/gitlab/issues/5741) or how they'd go about doing it. That's kind of what this repo is about.

A brief aside on the other tools that I looked at:
 * [Flawfinder](https://dwheeler.com/flawfinder/)
 * [Bandit](https://github.com/PyCQA/bandit)

So Bandit is pretty complicated making ASTs and the like. Flawfinder just looks for keywords that are on a blacklist (AFAIK). Something that has lately taken my interest is [Siderophile](https://github.com/trailofbits/siderophile).
